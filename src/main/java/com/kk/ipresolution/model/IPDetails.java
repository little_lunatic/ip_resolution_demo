package com.kk.ipresolution.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * the class of demo
 *
 * <p>
 * .
 *
 * @author little_lunatic
 * @date 2023-08-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IPDetails {
            private String ip;
            private String pro;
            private String proCode;
            private String city;
            private String cityCode;
            private String region;
            private String regionCode;
            private String addr;
            private String regionNames;
            private String err;
}
