package com.kk.ipresolution.config;

import com.kk.ipresolution.interceptor.IpUrlInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * the class of ip_resolution_demo
 *
 * <p>
 * .
 *
 * @author little_lunatic
 * @date 2024-02-04
 */
@Configuration
public class Config implements WebMvcConfigurer {

    @Autowired
    private IpUrlInterceptor ipUrlLimitInterceptor;

    //执行ip拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(ipUrlLimitInterceptor)
                // 拦截所有请求
                .addPathPatterns("/**");
    }

}
