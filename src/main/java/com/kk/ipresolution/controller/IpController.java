package com.kk.ipresolution.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kk.ipresolution.service.IpService;
import com.kk.ipresolution.utils.IPUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * the class of ip_resolution_demo
 *
 * <p>
 * .
 *
 * @author little_lunatic
 * @date 2023-08-24
 */
@RestController
@Api(tags = "IP地址解析Controller")
@ApiSupport(author = "little_lunatic")
public class IpController {

    @Autowired
    private IpService ipService;

    @GetMapping("/offline")
    public String offlineResolution(String ip){
        return ipService.offlineResolution(ip);
    }

    @GetMapping("/online")
    public String onlineResolution(String ip){
        return ipService.onlineResolution(ip);
    }
}
