package com.kk.ipresolution.service;

import com.kk.ipresolution.utils.IPUtil;
import org.springframework.stereotype.Service;

/**
 * the class of ip_resolution_demo
 *
 * <p>
 * .
 *
 * @author little_lunatic
 * @date 2023-08-24
 */
@Service
public class IpService {

    public String offlineResolution(String ip){
        return IPUtil.offlineResolutionIp(ip);
    }
    public String onlineResolution(String ip){
        return IPUtil.onlineResolutionIp(ip);
    }
}
