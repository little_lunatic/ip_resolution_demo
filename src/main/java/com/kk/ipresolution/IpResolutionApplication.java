package com.kk.ipresolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * the class of ip_resolution_demo
 *
 * <p>
 * .
 *
 * @author little_lunatic
 * @date 2023-08-24
 */
@SpringBootApplication
public class IpResolutionApplication {
    public static void main(String[] args) {
        SpringApplication.run(IpResolutionApplication.class, args);
    }
}
